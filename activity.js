/*
	Von Arce
	S32-Activity
*/

let http = require("http")

let port = 4000

http.createServer((request, response) => {
		if(request.url == "/" && request.method == "GET") {
			
			response.writeHead(200, {'Context-Type': 'application/json'})

			
			response.write('Welcome to booking system')
			response.end()

		}

		 if(request.url == "/profile" && request.method == "GET") {	  

			response.writeHead(200, {'Context-Type': 'application/json'})

			
			response.write('Welcome to your profile')
			response.end()

			}

			 if(request.url == "/courses" && request.method == "GET") {

					response.writeHead(200, {'Context-Type': 'application/json'})

					
					response.write('Heres our courses available')
					response.end()

			}

			 if(request.url == "/addcourse" && request.method == "POST") {

				response.writeHead(200, {'Context-Type': 'application/json'})

				
				response.write('Add a course to our resources')
				response.end()

			}


			if(request.url == "/updatecourse" && request.method == "PUT") {
						response.writeHead(200, {'Context-Type': 'application/json'})

				
				response.write('Update a course to our resources')
				response.end()
			}

			if(request.url == "/archivecourses" && request.method == "DELETE") {

					response.writeHead(200, {'Context-Type': 'application/json'})

				
				response.write('Archive courses to our resources')
				response.end()

			}

			else {

				response.writeHead(404, {'Context-Type': 'application/json'})

				
				response.write('Page not found')
				response.end()

			}

		}).listen(port) 

console.log(`Server is Running at localhost:${port}`);